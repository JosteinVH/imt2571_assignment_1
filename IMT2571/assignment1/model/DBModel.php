<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
		else
		{
            // Create PDO connection
      try{
            $this->db = new PDO('mysql:dbname=test;host=localhost', 'root', '');
              $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                      //Error reporting   //Throw exception
      }
      catch(PDOException $e){
        echo "Error!: " . $e->getMessage() . '<br>';
      }
    }
    }

    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		$booklist = array();
      try{
        $stmt = $this->db->prepare('SELECT * FROM book');
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) { //
            $booklist[] = new Book($row['title'], $row['author'], $row['description'], $row['id']);
          }
          return $booklist;
      } catch(PDOException $e){
        $e->getMessage();
      }
    }

    /** Function retrieving information about a given book in the collection.
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
    $book = null;
        try{
          $stmt = $this->db->prepare('SELECT * FROM book WHERE id = :id');
          $stmt->bindValue(':id',$id);
          $stmt->execute();
          $got_book = $stmt->fetch(PDO::FETCH_ASSOC);
            if($got_book) {
              $book = new Book ($got_book['title'], $got_book['author'], $got_book['description'], $got_book['id']);
              return $book;
            }
        }

        catch(PDOException $e) {
          $e->getMessage();
        }
    }

    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book) {
    //if(!empty($_POST['title']) && !empty($_POST['author'])){
            try{
              $stmt = $this->db->prepare('INSERT INTO book (title, author, description) VALUES (:tit, :auth, :des)');
              $stmt->bindValue(':tit', $book->title, PDO::PARAM_STR);
              $stmt->bindValue(':auth', $book->author, PDO::PARAM_STR);
              $stmt->bindValue(':des', $book->description, PDO::PARAM_STR);
              if($book->title != "" && $book->author != ""){
                $stmt->execute();
              }
              else {
                echo "Insert VALID values";
              }
              $book->id = $this->db->lastInsertId();
              }
            catch(PDOException $e){
              $e->getMessage();
            }
//}


  }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
        try {
          $stmt = $this->db->prepare("UPDATE book SET title = :tit, author = :auth, description = :des  WHERE id = :id");
		      $stmt->bindValue(':id', $book->id, PDO::PARAM_STR);
          $stmt->bindValue(':tit', $book->title, PDO::PARAM_STR);
          $stmt->bindValue(':auth', $book->author, PDO::PARAM_STR);
          $stmt->bindValue(':des', $book->description, PDO::PARAM_STR);
              if($book->title != "" && $book->author != ""){
              $stmt->execute();
            }
            else {
              //header("Location: http://localhost/IMT2571/assignment1/index.php?id=$book->id");
              echo "Insert VALID values";

            }
          }
          catch(PDOException $e){
          $e->getMessage();
          }




    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
      $stmt = $this->db->prepare('DELETE FROM book WHERE id = :id');
      $stmt->bindValue(':id', $id);
      $stmt->execute();

    }
}

?>
